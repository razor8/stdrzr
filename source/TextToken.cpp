//Copyright(c) 2017
//Authors: Fabian Wahlster
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com

//////////////////////////////////////////////////////////////////////////////
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute copies of the Software, and
//to permit persons to whom the Software is furnished to do so, subject to the
//following conditions:
//
//- Naming the author(s) of this software in any of the following locations:
//	About page, README file, credits.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#include "TextToken.h"
#include "FileStream.h"

namespace stdrzr
{
	TextToken::TextToken(bytestream& stream, std::string _key, bool keys_to_lower)
	{
		Initialize(stream, _key, keys_to_lower);
	}

	//---------------------------------------------------------------------------------------------------

	TextToken::TextToken(stdrzr::string filepath, std::string _key, bool keys_to_lower, std::ios_base::openmode mode)
	{
		fbytestream file(filepath, mode);
		if (file.is_open())
		{
			std::unique_ptr<bytestream> bstream = file.make_bytestream();
			if (bstream != nullptr)
			{
				Initialize(*bstream, _key, keys_to_lower);
			}
		}
	}
	//---------------------------------------------------------------------------------------------------

	std::string TextToken::read_value(bytestream& stream, char separator)
	{
		char c = read_whitespaces<char>(stream);
		std::string ret;
		// remove whitespaces, tabs, linefeed
		if (c == separator)
		{
			while (stream.good() && (c = stream.get<char>()) != separator)
			{
				ret += c;
			}
		}
		else
		{
			ret += c;
		}
		return ret;
	}
	//---------------------------------------------------------------------------------------------------

	void TextToken::Initialize(bytestream & stream, std::string _key, bool keys_to_lower)
	{
		bool bTokenAsValue = false;

		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		this->m_key = _key;
		std::string token, value;

		while (stream.good())
		{
			token = read_value(stream);
			if (keys_to_lower)
			{
				stdrzr::to_lower(token);
			}

			if (token == "}") 
			{
				break;
			}
			else if (token == "]")
			{
				bTokenAsValue = false;
				continue;
			}
			else if (token == ";" || token == "#")
			{//comment
				/*value = */skip_until(stream, '\n'); // skip the rest of the line
				continue;
			}
			else if(bTokenAsValue == false)
			{
				value = read_value(stream);
			}

			if (token == _key)
			{
				continue;
			}
			else if (value == "{")
			{
				children.insert(std::pair<std::string, TextToken >(token, TextToken(stream, token, keys_to_lower)));
			}
			else if (value == "[")
			{
				value = token;
				bTokenAsValue = true;
				continue;
			}
			else
			{
				if (bTokenAsValue == true) 
				{
					values.insert(std::pair<std::string, std::string>(value, token));
				}
				else 
				{
					values.insert(std::pair<std::string, std::string>(token, value));
				}
			}
		}
	}
	//---------------------------------------------------------------------------------------------------

	bool TextToken::getToken(std::string _key, TextToken& out, bool keys_to_lower)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		} 

		std::multimap<std::string, TextToken>::iterator itr = children.find(_key);

		if (itr == children.end())
		{
			return false;
		}

		out = itr->second;
		return true;
	}
	//---------------------------------------------------------------------------------------------------

	bool TextToken::getTokenConst(std::string _key, TextToken& out, bool keys_to_lower) const
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		std::multimap<std::string, TextToken>::const_iterator itr = children.find(_key);

		if (itr == children.end())
		{
			return false;
		}

		out = itr->second;
		return true;
	}
	//---------------------------------------------------------------------------------------------------

	std::vector<TextToken> TextToken::getAllTokens(void) const
	{
		std::vector<TextToken> ret;
		std::multimap<std::string, TextToken>::const_iterator itr;

		for (itr = children.cbegin(); itr != children.cend(); ++itr){
			ret.push_back(itr->second);
		}

		return ret;
	}

	//---------------------------------------------------------------------------------------------------

	bool TextToken::getTokenFromPath(std::string path, TextToken& out, bool keys_to_lower, char separator)
	{
		size_t pidx, idx = path.find_first_of(separator, 0);

		if (idx >= path.size())
		{
			return false;
		}		

		if (getToken(path.substr(0, idx), out, keys_to_lower) == false)
		{
			return false;
		}		

		pidx = idx; bool b = true;
		TextToken tmp;
		while ((idx = path.find_first_of(separator, idx + 1)) < path.size() && b)
		{
			b = out.getToken(path.substr(++pidx, idx - pidx), tmp, keys_to_lower);
			out = tmp;
			pidx = idx;
		}

		if (++pidx < path.size() && b)
		{
			b = out.getToken(path.substr(pidx), tmp, keys_to_lower);
			out = tmp;
			return b;
		}

		return true;
	}

	//---------------------------------------------------------------------------------------------------

	std::string TextToken::getValue(std::string _key, const std::string& _sDefaultValue, bool keys_to_lower) const
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		std::multimap<std::string, std::string>::const_iterator itr = values.find(_key);
		if (itr == values.cend())
		{
			return _sDefaultValue;
		}
		return itr->second;
	}

	//---------------------------------------------------------------------------------------------------
	std::vector<std::string> TextToken::getAllValues(std::string _key, bool keys_to_lower) const
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		std::vector<std::string> ret;
		std::pair<std::multimap<std::string, std::string>::const_iterator, std::multimap<std::string, std::string>::const_iterator> itr = values.equal_range(_key);

		for (std::multimap<std::string, std::string>::const_iterator it = itr.first; it != itr.second; ++it){
			ret.push_back(it->second);
		}

		return ret;
	}
	//---------------------------------------------------------------------------------------------------

	void TextToken::remove(const std::string& _sKey, bool _bRange)
	{
		std::pair<std::multimap<std::string, std::string>::iterator, std::multimap<std::string, std::string>::iterator> itr = values.equal_range(_sKey);

		for (std::multimap<std::string, std::string>::iterator it = itr.first; it != itr.second;)
		{
			it = values.erase(it);

			if (_bRange == false)
			{
				break;
			}			
		}
	}
	//---------------------------------------------------------------------------------------------------

	void TextToken::serialize(bytestream& stream, std::string tabs) const
	{
		stream.put(tabs + "\"" + this->m_key + "\"\n" + tabs + "{\n", false);

		for (std::multimap<std::string, std::string>::const_iterator itr = values.cbegin(); itr != values.cend(); ++itr)
		{
			stream.put(tabs + "\t\"" + itr->first + "\"\t\t\"" + itr->second + "\"\n", false);
		}

		for (std::multimap<std::string, TextToken>::const_iterator itr = children.cbegin(); itr != children.cend(); ++itr)
		{
			itr->second.serialize(stream, tabs + "\t");
		}

		stream.put(tabs + "}\n", false);
	}
	//---------------------------------------------------------------------------------------------------

	bool TextToken::serialize(stdrzr::string filepath, std::ios_base::openmode mode) const
	{
		fbytestream file(filepath, mode);

		if (file.is_open())
		{
			bytes buffer;
			bytestream bstream(buffer);

			serialize(bstream);

			file.put<bytes>(buffer);

			file.close();

			return true;
		}
		else
		{
			return false;
		}
	}
	//---------------------------------------------------------------------------------------------------
}