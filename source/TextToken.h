//Copyright(c) 2017
//Authors: Fabian Wahlster
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com

//////////////////////////////////////////////////////////////////////////////
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute copies of the Software, and
//to permit persons to whom the Software is furnished to do so, subject to the
//following conditions:
//
//- Naming the author(s) of this software in any of the following locations:
//	About page, README file, credits.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#ifndef TEXTTOKEN_H
#define TEXTTOKEN_H

#include "ByteStream.h"
#include "StringHelpers.h"
#include "Vector.h"
#include <map>
#include <iomanip>

namespace stdrzr
{
	template<typename T>
	inline std::ostringstream& operator<<(typename std::enable_if<std::is_enum<T>::value, std::ostringstream>::type& stream, const T& e)
	{
		stream /*<< "0x"*/ << std::hex << static_cast<uint64_t>(e);
		//stream << std::to_string(static_cast<typename std::underlying_type<T>::type>(e));
		return stream;
	}

	// read uint8_t as integral, not char
	inline std::istringstream& operator>>(std::istringstream& stream, uint8_t& _u)
	{
		uint16_t uProxy = 0u;
		stream >> std::hex >> uProxy;
		_u = static_cast<uint8_t>(uProxy);
		return stream;
	}

	template<typename T>
	inline std::istringstream& operator>>(typename std::enable_if<std::is_enum<T>::value, std::istringstream>::type& stream, T& e)
	{
		/*std::underlying_type<T>::type*/ uint64_t Proxy = {};

		stream >> std::hex >> Proxy;
		e = static_cast<T>(Proxy);

		return stream;
	}

class TextToken
{
private:
	std::string m_key;//name
	std::multimap<std::string,TextToken> children;
	std::multimap<std::string,std::string> values;

	std::string read_value(bytestream& stream,char seperator = '\"');

public:
	//default constructor
	TextToken(const std::string& _sKey = {}) : m_key(_sKey) {};
	//parse key-value from stream
	TextToken(bytestream& stream, std::string key, bool keys_to_lower = false);
	//parse kv from file
	TextToken(stdrzr::string filepath, std::string key, bool keys_to_lower = false, std::ios_base::openmode mode = std::ios_base::binary | std::ios_base::in);

	inline bool IsEmpty() const { return children.empty() && values.empty(); }
	inline const std::string& getKey(void) const { return m_key; }
	inline void setKey(const std::string& _sKey) { m_key = _sKey; }
	inline size_t getChildCount() const { return children.size(); }
	inline size_t getValueCount() const { return values.size(); }

	//populate token from bytestream
	void Initialize(bytestream& stream, std::string key, bool keys_to_lower);
	//write key-value tree to stream
	void serialize(bytestream& stream, std::string tabs = {}) const;

	//write key-value tree to file
	bool serialize(stdrzr::string filepath, std::ios_base::openmode mode = std::ios_base::out | std::ios_base::trunc) const;
	//---------------------------------------------------------------------------------------------------

	//get primitive value by key
	template <class T> T get(std::string key, const T& _DefaultValue = {}, bool keys_to_lower = false) const
	{
		T ret = _DefaultValue;
		if (keys_to_lower)
		{
			stdrzr::to_lower(key);
		}

		std::multimap<std::string,std::string>::const_iterator itr = values.find(key);
		if (itr == values.cend())
		{
			return ret;
		}
		
		std::istringstream stream(itr->second,std::ios_base::in);
		stream >> ret;

		return ret;
	}
	//---------------------------------------------------------------------------------------------------

	template <class T, int Dim> VecType<T, Dim> getVector(std::string key, const VecType<T, Dim>& _DefaultValue = {}, bool keys_to_lower = false) const
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(key);
		}

		VecType<T, Dim> ret = _DefaultValue;
		std::multimap<std::string, std::string>::const_iterator itr = values.find(key);

		if (itr == values.cend())
		{
			return ret;
		}

		std::istringstream stream(itr->second, std::ios_base::in);

		for (int i = 0; i < Dim; ++i)
		{
			stream >> ret[i];
		}
	
		return ret;
	}
	//---------------------------------------------------------------------------------------------------

	//get primitive value from path
	template <class T> T getFromPath(std::string path, std::string key, const T& _DefaultValue = {}, bool keys_to_lower = false) const
	{
		TextToken token;
		if (getTokenFromPath(path, object, keys_to_lower))
		{
			if (keys_to_lower)
			{
				stdrzr::to_lower(key);
			}
			return token.get<T>(key, _DefaultValue, keys_to_lower);
		}

		return _DefaultValue;
	}
	//---------------------------------------------------------------------------------------------------

	//get all primitive values with the specified key
	template <class T> std::vector<T> getAll(std::string key, bool keys_to_lower = false) const
	{
		T val = T();
		std::vector<T> ret;

		if (keys_to_lower)
		{
			stdrzr::to_lower(key);
		}

		auto itr = values.equal_range(key);

		for(std::multimap<std::string,std::string>::const_iterator it = itr.first; it != itr.second;++it)
		{
			std::istringstream stream(it->second,std::ios_base::in);
			stream >> val;
			ret.push_back(val);
		}	

		return ret;
	}
	//---------------------------------------------------------------------------------------------------

	//get all primitive values from this token
	template <class T> std::vector<T> getAll(void) const
	{
		T val = T();
		std::vector<T> ret;

		for(std::multimap<std::string, std::string>::const_iterator itr = values.begin(); itr != values.end();++itr)
		{
			std::istringstream stream(itr->second,std::ios_base::in);
			//from_hex<T>(stream, itr->second);
			stream >> val;
			ret.push_back(val);
		}

		return ret;
	}
	//---------------------------------------------------------------------------------------------------

	//get all key value pairs (with the value being a string) from this token
	template <class T> std::vector<std::pair<T,std::string> > getAllPairs(void) const
	{
		T key = T();
		std::vector<std::pair<T,std::string> > ret;

		for(std::multimap<std::string, std::string>::const_iterator itr = values.begin(); itr != values.end();++itr)
		{
			std::istringstream stream(itr->first,std::ios_base::in);
			stream >> key;
			ret.push_back({ key,itr->second });
		}

		return ret;
	}
	//---------------------------------------------------------------------------------------------------

	//get all primitive key value pairs from this token
	template <class KeyType, class ValueType> std::vector<std::pair<KeyType,ValueType> > getAllPairs(void) const
	{
		KeyType key = {};
		ValueType val = {};
		std::vector<std::pair<KeyType, ValueType> > ret;

		for(std::multimap<std::string, std::string>::const_iterator itr = values.begin(); itr != values.end();++itr)
		{
			std::istringstream stream(itr->first + ' ' + itr->second,std::ios_base::in);
			stream >> key >> val;
			ret.push_back({ key,val });
		}

		return ret;
	}
	//---------------------------------------------------------------------------------------------------

	//get the raw value data (string) with the corresponding key
	std::string getValue(std::string _key, const std::string& _sDefaultValue = {}, bool keys_to_lower = false) const;

	//get all raw values (string) with the corresponding key
	std::vector<std::string> getAllValues(std::string _key, bool keys_to_lower = false) const;

	//get the child token with the corresponding key
	bool getToken(std::string _key, TextToken& out, bool keys_to_lower = false);

	//get the const child token with the corresponding key
	bool getTokenConst(std::string _key, TextToken& out, bool keys_to_lower = false) const;
	//---------------------------------------------------------------------------------------------------

	//get all child tokens with the corresponding key
	inline std::pair<std::multimap<std::string,TextToken>::iterator, std::multimap<std::string,TextToken>::iterator > getAllTokens(std::string _key,bool keys_to_lower = false)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		return children.equal_range(_key);
	}
	//---------------------------------------------------------------------------------------------------

	//get all const child tokens with the corresponding key
	inline std::pair<std::multimap<std::string, TextToken>::const_iterator, std::multimap<std::string, TextToken>::const_iterator > getAllTokens(std::string _key, bool keys_to_lower = false) const
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		return children.equal_range(_key);
	}
	//---------------------------------------------------------------------------------------------------

	//get all child tokens from this token
	std::vector<TextToken> getAllTokens(void) const;

	//get the token using a complete path (e.g. AppState->getTokenFromPath("UserConfig/CheckGuid");
	bool getTokenFromPath(std::string path, TextToken& out, bool keys_to_lower = false, char separator = '/');

	//---------------------------------------------------------------------------------------------------

	template <class T>
	void SetPrecision(std::ostringstream& ss) const {}

	template <>
	void SetPrecision<float>(std::ostringstream& ss) const
	{
		ss.precision(std::numeric_limits<float>::max_digits10);
	}

	template <>
	void SetPrecision<double>(std::ostringstream& ss) const
	{
		ss.precision(std::numeric_limits<double>::max_digits10);
	}
	//---------------------------------------------------------------------------------------------------


	//adds primitive value using the specified key
	template <class T>
	void add(std::string _key, const T& value, bool keys_to_lower = false, bool _bNoDuplicates = false)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		if (_bNoDuplicates && values.count(_key) > 0)
		{
			return;
		}

        std::ostringstream ss;

		// set maximum precision based on type
		SetPrecision<T>(ss);

		ss << value;

		values.insert({ _key,ss.str() });
	}
	//---------------------------------------------------------------------------------------------------

	template <> void add(std::string _key, const std::string& value, bool keys_to_lower, bool _bNoDuplicates)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		if (_bNoDuplicates && values.count(_key) > 0)
		{
			return;
		}

		values.insert({ _key, value });
	}
	//---------------------------------------------------------------------------------------------------

	template<class T, class itr = std::iterator<std::forward_iterator_tag, T>>
	void addContainer(std::string _key, itr _begin, itr _end, bool keys_to_lower = false, bool _bNoDuplicates = false)
	{
		if (std::distance(_begin, _end) == 0u)
		{
			return;
		}

		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		if (_bNoDuplicates && values.count(_key) > 0)
		{
			return;
		}

		for (;_begin != _end; ++_begin)
		{
			std::ostringstream ss;

			// set maximum precision based on type
			SetPrecision<T>(ss);

			ss << *_begin;

			values.insert({ _key,ss.str() });
		}
	}
	//---------------------------------------------------------------------------------------------------

	void remove(const std::string& _sKey, bool _bRange = false);

	//replaces the primitive value at the specified key, or adds the value if the key doesnt exist
	template <class T> void set(std::string _key, const T& value, bool keys_to_lower = false)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		std::multimap<std::string,std::string>::iterator itr = values.find(_key);

		std::ostringstream ss;
		ss << value;

		if(itr != values.end())
		{		
			itr->second = ss.str();
		}
		else
		{
			values.insert({ _key, ss.str() });
		}
	}
	//---------------------------------------------------------------------------------------------------

	// avoid uint8_t printed as a char
	template <> void set(std::string _key, const uint8_t& value, bool keys_to_lower)
	{
		std::ostringstream ss;
		ss /*<< "0x"*/ << std::hex << static_cast<uint16_t>(value);
		set(_key, ss.str()/*std::to_string(value)*/, keys_to_lower);
	}

	void set(std::string _key, const uint8_t& value)
	{
		std::ostringstream ss;
		ss /*<< "0x"*/ << std::hex << static_cast<uint16_t>(value);
		set(_key, ss.str()/*std::to_string(value)*/, false);
	}
	//---------------------------------------------------------------------------------------------------

	template <class T, int Dim> void setVector(std::string _key, const VecType<T, Dim>& value, bool keys_to_lower = false)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		std::ostringstream ss;
		
		for (int i = 0; i < Dim; ++i)
		{
			ss << value[i];
			if (i < Dim - 1)
			{
				ss << " ";
			}
		}

		std::multimap<std::string, std::string>::iterator itr = values.find(_key);

		if (itr != values.end())
		{
			itr->second = ss.str();
		}
		else
		{
			values.insert({ _key, ss.str() });
		}
	}
	//---------------------------------------------------------------------------------------------------

	//string version
	template <> void set(std::string _key, const std::string& value, bool keys_to_lower)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		std::multimap<std::string, std::string>::iterator itr = values.find(_key);

		if (itr != values.end())
		{
			itr->second = value;
		}
		else
		{
			values.insert({ _key, value });
		}
	}
	//---------------------------------------------------------------------------------------------------

	//adds a token using the specified key
	void addToken(std::string _key,const TextToken& token, bool keys_to_lower = false)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		children.insert({ _key,token });
	}
	//---------------------------------------------------------------------------------------------------

	//replaces the token at the specified key
	void setToken(std::string _key, const TextToken& token, bool keys_to_lower = false)
	{
		if (keys_to_lower)
		{
			stdrzr::to_lower(_key);
		}

		std::multimap<std::string,TextToken>::iterator itr = children.find(_key);

		if (itr != children.end())
		{
			itr->second = token;
		}
		else
		{
			children.insert({ _key,token });
		}
	}
};

} // stdrzr
#endif // TEXTTOKEN_H
