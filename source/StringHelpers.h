//Copyright(c) 2017
//Authors: Fabian Wahlster
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com

//////////////////////////////////////////////////////////////////////////////
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute copies of the Software, and
//to permit persons to whom the Software is furnished to do so, subject to the
//following conditions:
//
//- Naming the author(s) of this software in any of the following locations:
//	About page, README file, credits.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#ifndef STRINGHELPERS_H
#define STRINGHELPERS_H

#include "ByteStream.h"
#include "String.h"
#include <vector>
#include <algorithm>
#include <locale>

namespace stdrzr
{
	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> to_lower(const std::basic_string<char_t>& str)
	{
		std::basic_string<char_t> out = str;
		std::transform(out.begin(), out.end(), out.begin(), ::tolower);
		return out;
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> to_lower(const char_t* str)
	{
		return to_lower(std::basic_string<char_t>(str));
	}

	template <typename char_t = base_char_t>
	inline void to_lower(std::basic_string<char_t>& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	}

	template <typename T, typename char_t = base_char_t>
	T FromString(const std::basic_string<char_t>& str)
	{
		T ret = T();
		std::basic_stringstream<char_t> stream(str, std::ios_base::in);
		stream >> ret;
		return ret;
	}
	
	template<typename T>
	inline void skip_until(bytestream& stream,const T& b)
	{
		while (stream.good())
		{
			if (stream.get<T>() == b) break;
		}
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> trim_right(const std::basic_string<char_t>& str)
	{
		int64_t uPos = str.size() - 1;
		for (; uPos > 0 && std::isspace(str[uPos], std::locale::classic()); --uPos) {};
		return str.substr(0, uPos+1);
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> trim_left(const std::basic_string<char_t>& str)
	{
		int64_t uPos = 0;
		for (; uPos < str.size() && std::isspace(str[uPos], std::locale::classic()); ++uPos) {};
		return str.substr(uPos);
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> trim(const std::basic_string<char_t>& str)
	{
		return trim_left(trim_right(str));
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> read_until(bytestream& stream, char_t separator)
	{		
		char_t c = {};
		std::basic_string<char_t> ret;
		while (stream.good() && (c = stream.get<char_t>()) != separator) { ret += c; }
		return ret;
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> read_until(std::basic_stringstream<char_t>& stream, char_t separator)
	{
		char_t c = {};
		std::basic_string<char_t> ret;
		while (stream.good() && (c = stream.get()) != separator) { ret += c; }
		return ret;
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> read_until(std::basic_stringstream<char_t>& stream, const std::basic_string<char_t>& delimiter)
	{
		std::basic_string<char_t> sOut;
		if (delimiter.empty())
		{
			return sOut;
		}
		
		size_t uAlloc = delimiter.size();
		sOut.resize(delimiter.size());
		stream.read(&sOut[0], uAlloc);

		size_t uOff = 0;

		while (sOut.find(delimiter, uOff) == std::string::npos && stream.good())
		{
			uOff = sOut.size();

			sOut.resize(uOff + uAlloc);

			stream.read(&sOut[uOff], uAlloc);

			if (uOff > 0)
			{
				--uOff;
			}
		}

		return sOut;
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> read_until(std::basic_stringstream<char_t>& stream, const char_t* delimiter)
	{
		return read_until(stream, std::basic_string<char_t>(delimiter));
	}

	template <typename char_t = base_char_t>
	inline std::vector<std::basic_string<char_t>> split(const std::basic_string<char_t>& str, char_t separator)
	{
		std::vector<std::basic_string<char_t>> tokens;
		std::basic_stringstream<char_t> stream(str);

		std::basic_string<char_t> token;
		while (std::getline(stream, token, separator))
		{
			tokens.push_back(token);
		}

		return tokens;
	}

	template <typename char_t = base_char_t>
	inline std::vector<std::basic_string<char_t>> split(const char_t* str, char_t separator)
	{
		return split(std::basic_string<char_t>(str));
	}

	template <typename char_t = base_char_t>
	inline size_t find_first_of(const std::basic_string<char_t>& _sInput, const std::vector<std::basic_string<char_t>>& _sFind, _Out_ size_t& _uIndex, const size_t& _uPos = 0u)
	{
		size_t uFirst = std::string::npos;
		size_t uCur = std::string::npos;

		const size_t uSize = _sFind.size();
		for (size_t i = 0u; i < uSize; ++i)
		{			
			uCur = _sInput.find(_sFind.at(i), _uPos);
			if (uCur < uFirst)
			{
				uFirst = uCur;
				_uIndex = i;
			}
		}

		return uFirst;
	}

	// set _uStart = 0 to search from the beginning of the string
	template <typename char_t = base_char_t>
	inline bool get_body(const std::basic_string<char_t>& _sInput, _Inout_ size_t& _uStart, _Out_ size_t& _uEnd, const std::basic_string<char_t>& _sOpen, const std::basic_string<char_t>& _sClose)
	{
		const size_t uMinOpen = _sInput.find(_sOpen, _uStart);

		if (uMinOpen == std::string::npos)
		{
			_uStart = std::string::npos;
			_uEnd = std::string::npos;
			return false;
		}

		const std::vector<std::basic_string<char_t>> Tags = { _sOpen, _sClose };

		size_t uMaxClose = uMinOpen;
		size_t uCount = 1u;

		for (size_t uCurPos = uMinOpen, uIndex = 0u;
			uCount > 0u && uCurPos != std::string::npos;)
		{
			uCurPos = find_first_of<char_t>(_sInput, Tags, uIndex, uCurPos + 1);

			if (uCurPos == std::string::npos)
				break;

			if (uIndex == 0u) // open tag
			{
				++uCount;
			}
			else if (uIndex == 1u) // close tag
			{
				uMaxClose = uCurPos;
				--uCount;
			}
		}

		_uStart = uMinOpen;
		_uEnd = uMaxClose;

		return uMinOpen != std::string::npos && uMaxClose != std::string::npos;
	}

	template <typename char_t = base_char_t>
	inline bool get_body(const std::basic_string<char_t>& _sInput, _Inout_ size_t& _uStart, _Out_ size_t& _uEnd, const char_t* _sOpen = ST(char_t, "{"), const char_t* _sClose = ST(char_t,"}"))
	{
		return get_body(_sInput, _uStart, _uEnd, std::basic_string<char_t>(_sOpen), std::basic_string<char_t>(_sClose));
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> get_body(const std::basic_string<char_t>& _sInput, const std::basic_string<char_t>& _sOpen, const std::basic_string<char_t>& _sClose)
	{
		size_t uStart = 0;
		size_t uEnd = 0;

		if (get_body(_sInput, uStart, uEnd, _sOpen, _sClose))
		{
			return _sInput.substr(uStart + _sOpen.size(), uEnd - uStart - _sOpen.size());
		}
		
		return std::basic_string<char_t>();
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> get_body(const std::basic_string<char_t>& _sInput, const char_t* _sOpen = ST(char_t, "{"), const char_t* _sClose = ST(char_t, "}"))
	{
		return get_body(_sInput, std::basic_string<char_t>(_sOpen), std::basic_string<char_t>(_sClose));
	}

	template <typename char_t = base_char_t>
	inline std::vector<std::basic_string<char_t>> split(const std::basic_string<char_t>& str, const std::basic_string<char_t>& separator)
	{
		std::vector<std::basic_string<char_t>> tokens;
		
		int64_t uPos = 0;
		int64_t uNext = str.find(separator);

		while (uNext - uPos > 0)
		{
			tokens.push_back(str.substr(uPos, uNext - uPos));
			uPos = uNext + separator.size();
			uNext = str.find(separator,uPos);
		}

		if (uPos != -1)
		{
			tokens.push_back(str.substr(uPos));
		}

		return tokens;
	}

	template <typename char_t = base_char_t>
	inline std::vector<std::basic_string<char_t>> split(const std::basic_string<char_t>& str, const char_t* separator)
	{
		return split(str, std::basic_string<char_t>(separator));
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> get_right_of(const std::basic_string<char_t>& str, const std::basic_string<char_t>& find, bool _bCaseSensitive = true)
	{
		if (_bCaseSensitive)
		{
			return str.substr(str.rfind(find) + find.length(), string::npos);
		}
		else
		{
			return str.substr(to_lower(str).rfind(to_lower(find)) + find.length(), string::npos);
		}
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> get_right_of(const std::basic_string<char_t>& str, const char_t* find, bool _bCaseSensitive = true)
	{
		return get_right_of(str, std::basic_string<char_t>(find), _bCaseSensitive);
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> get_left_of(const std::basic_string<char_t>& str, const std::basic_string<char_t>& find, bool _bCaseSensitive = true, bool _bReverse = false)
	{
		if (_bCaseSensitive)
		{
			return str.substr(0, _bReverse ? str.rfind(find) : str.find(find));
		}
		else
		{
			return str.substr(0, _bReverse ? to_lower(str).rfind(to_lower(find)) : to_lower(str).find(to_lower(find)));
		}		
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> get_left_of(const std::basic_string<char_t>& str, const char_t* find, bool _bCaseSensitive = true, bool _bReverse = false)
	{
		return get_left_of(str, std::basic_string<char_t>(find),_bCaseSensitive, _bReverse);
	}

	template <typename char_t = base_char_t>
	inline bool contains(const std::basic_string<char_t>& str, const std::basic_string<char_t>& find, bool _bCaseSensitive = true)
	{
		if (_bCaseSensitive) 
		{
			return str.find(find, 0) != string::npos;
		}
		else
		{
			return to_lower(str).find(to_lower(find), 0) != string::npos;
		}		
	}

	template <typename char_t = base_char_t>
	inline bool contains(const std::basic_string<char_t>& str, const char_t* find, bool _bCaseSensitive = true)
	{
		return contains(str, std::basic_string<char_t>(find), _bCaseSensitive);
	}

	template <typename char_t = base_char_t>
	inline bool starts_with(const std::basic_string<char_t>& str, const std::basic_string<char_t>& find, bool _bCaseSensitive = true)
	{
		if (_bCaseSensitive)
		{
			return str.find(find, 0) == 0;
		}
		else
		{
			return to_lower(str).find(to_lower(find), 0) == 0;
		}		
	}

	template <typename char_t = base_char_t>
	inline bool starts_with(const std::basic_string<char_t>& str, const char_t* find, bool _bCaseSensitive = true)
	{
		return starts_with(str, std::basic_string<char_t>(find), _bCaseSensitive);
	}

	template <typename char_t = base_char_t>
	inline bool ends_with(const std::basic_string<char_t>& str, const std::basic_string<char_t>& find, bool _bCaseSensitive = true)
	{
		if (_bCaseSensitive)
		{
			return str.rfind(find) == str.size() - find.size();
		}
		else
		{
			return to_lower(str).rfind(to_lower(find)) == str.size() - find.size();
		}
	}

	template <typename char_t = base_char_t>
	inline bool ends_with(const std::basic_string<char_t>& str, const char_t* find, bool _bCaseSensitive = true)
	{
		return ends_with(str, std::basic_string<char_t>(find), _bCaseSensitive);
	}

	template <typename char_t = base_char_t>
	inline void remove(std::basic_string<char_t>& str, char_t rm)
	{
		str.erase(std::remove(str.begin(), str.end(), rm), str.end());
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> remove(const std::basic_string<char_t>& str, char_t rm)
	{
		std::basic_string<char_t> ret(str);
		remove(ret, rm);
		return ret;
	}

	template <typename char_t = base_char_t>
	inline void remove(std::basic_string<char_t>& str, const std::basic_string<char_t>& rm)
	{
		size_t uFound = str.find(rm, 0);
		while (uFound != std::string::npos)
		{
			str.erase(uFound, rm.size());
			uFound = str.find(rm, 0);
		}		
	}

	template <typename char_t = base_char_t>
	inline void remove(std::basic_string<char_t>& str, const char_t* rm)
	{
		remove(str, std::basic_string<char_t>(rm));
	}
	
	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> remove(const std::basic_string<char_t>& str, const std::basic_string<char_t>& rm)
	{
		std::basic_string<char_t> ret(str);
		remove(ret, rm);
		return ret;
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> remove(const std::basic_string<char_t>& str, const char_t* rm)
	{
		std::basic_string<char_t> ret(str);
		remove(ret, rm);
		return ret;
	}

	template <typename char_t = base_char_t>
	inline void remove_any(std::basic_string<char_t>& str, const std::basic_string<char_t>& rm)
	{
		size_t uFound = str.find_first_of(rm, 0);
		while (uFound != std::string::npos)
		{
			str.erase(uFound, 1);
			uFound = str.find_first_of(rm, 0);
		}
	}

	template <typename char_t = base_char_t>
	inline void remove_any(std::basic_string<char_t>& str, const char_t* rm)
	{
		return remove_any(str, std::basic_string<char_t>(rm));
	}

	template <typename char_t = base_char_t>
	inline void remove_whitespaces(std::basic_string<char_t>& str)
	{
		str.erase(std::remove_if(str.begin(), str.end(), [](const char_t& ch) -> bool { return std::isspace<char_t>(ch, std::locale::classic()); }), str.end());
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> remove_whitespaces(const std::basic_string<char_t>& str)
	{
		std::basic_string<char_t> sRet(str);
		remove_whitespaces(sRet);
		return sRet;
	}

	template <typename char_t = base_char_t>
	inline void replace_extension(_Inout_ std::basic_string<char_t>& _sFilePath, const std::basic_string<char_t>& _sNewExtension)
	{
		size_t pos = _sFilePath.rfind(ST(char_t, "."));

		if (pos == string::npos)
		{
			_sFilePath.append(ST(char_t, ".") + _sNewExtension);
		}
		else
		{
			_sFilePath = _sFilePath.substr(0, pos + 1) + _sNewExtension;
		}
	}

	template <typename char_t = base_char_t>
	inline void replace_extension(_Inout_ std::basic_string<char_t>& _sFilePath, const char_t* _sNewExtension)
	{
		replace_extension(_sFilePath, std::basic_string<char_t>(_sNewExtension));
	}

	template <typename char_t = base_char_t>
	inline void replace(std::basic_string<char_t>& str, const std::basic_string<char_t>& find, const std::basic_string<char_t>& replace)
	{
		size_t uPos = str.find(find);

		while (uPos != std::string::npos)
		{
			str.replace(uPos, find.size(), replace);

			uPos = str.find(find, uPos + 1);
		}
	}

	template <typename char_t = base_char_t>
	inline void replace(std::basic_string<char_t>& str, const char_t* pFind, const char_t* pReplace)
	{
		replace(str, std::basic_string<char_t>(pFind), std::basic_string<char_t>(pReplace));
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> replace(const std::basic_string<char_t>& str, const std::basic_string<char_t>& sFind, const std::basic_string<char_t>& sReplace)
	{
		std::basic_string<char_t> ret(str);
		replace(ret, sFind, sReplace);
		return ret;
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> replace(const std::basic_string<char_t>& str, const char_t* pFind, const char_t* pReplace)
	{
		std::basic_string<char_t> ret(str);
		replace(ret, std::basic_string<char_t>(pFind), std::basic_string<char_t>(pReplace));
		return ret;
	}

	template <typename char_t = base_char_t>
	inline void skip_whitespaces(std::basic_stringstream<char_t>& stream)
	{
		int c = 0;
		bool b = true;
		while (stream.good() && b)
		{
			c = stream.get();
			if (std::isspace<char_t>(static_cast<char_t>(c), std::locale::classic()))
			{
				b = true;
				continue;
			}
			else
			{
				b = false;
				stream.unget();
				break;
			}
		}
	}

	template <typename char_t = base_char_t>
	inline char_t read_whitespaces(bytestream& stream)
	{
		char_t c = 0;
		bool b = true;
		while (stream.good() && b)
		{
			c = stream.get<char_t>();
			if (std::isspace<char_t>(c, std::locale::classic()))
			{
				b = true;
				continue;
			}
			else
			{
				b = false;
			}
		}
		return c;
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> read_token(bytestream& stream)
	{
		char_t c = 0;
		bool token = false;
		bool b = true;
		std::basic_string<char_t> ret;
		while (stream.good() && b)
		{
			c = stream.get<char_t>();
			if (std::isspace<char_t>(c, std::locale::classic()))
			{
				if (token == true) b = false;
				continue;
			}
			else
			{
				ret += c;
				token = true;
			}
		}
		return ret;
	}

	template <typename char_t = base_char_t>
	inline std::basic_string<char_t> read_token(std::basic_stringstream<char_t>& stream)
	{
		int c = 0;
		bool token = false;
		bool b = true;
		std::basic_string<char_t> ret;
		while (stream.good() && b)
		{
			c = stream.get();
			if (c == -1 || std::isspace<char_t>(c, std::locale::classic()))
			{
				if (token == true) b = false;
				continue;
			}
			else
			{
				ret += c;
				token = true;
			}
		}
		return ret;
	}
}

#endif