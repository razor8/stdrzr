//Copyright(c) 2017
//Authors: Fabian Wahlster
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com

//////////////////////////////////////////////////////////////////////////////
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files(the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute copies of the Software, and
//to permit persons to whom the Software is furnished to do so, subject to the
//following conditions:
//
//- Naming the author(s) of this software in any of the following locations:
//	About page, README file, credits.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#ifndef DEBUG_H
#define DEBUG_H

#ifndef _DEBUG
	//RELEASE CONFIGURATION
#ifndef NDEBUG
	#define NDEBUG //suppress assert()
#endif
	#define HInitDebug()
	#define HDEBUGNAME(_name)
#else
	//DEBUG CONFIGURATION
#ifndef _CRTDBG_MAP_ALLOC
	#define _CRTDBG_MAP_ALLOC
#endif	
	#include <stdlib.h>
	#include <crtdbg.h>
		static void HInitDebug(void){		
			_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
			_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );
		}
	#ifndef DBG_NEW
		#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ ) 
		#define new DBG_NEW 
	#endif

	//only use this when _DEBUG is defined!
#define HDEBUGNAME(_name) const TCHAR * getDebugName(void){return TEXT(_name);}

#endif //_DEBUG

#define	WIN32_LEAN_AND_MEAN
#include <Windows.h>
//DXTrace from dxerr.h is deprecated, use FormatMessage instead
#ifndef HR
#define HR(x) \
	{\
		if(FAILED(x)){\
			LPTSTR msg; \
			if(FormatMessage((FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS), NULL, (HRESULT)x, 0, (LPTSTR)&msg, 0, NULL) != 0){ \
				switch (MessageBox(NULL, msg, TEXT(#x) , MB_ABORTRETRYIGNORE | MB_ICONERROR | MB_DEFBUTTON3)){ \
				case IDIGNORE: break; \
				case IDABORT: DebugBreak(); break; \
				case IDRETRY:	break; \
				default:       break;\
				}\
			LocalFree(msg);\
			}\
		}\
	}
#endif

#include <assert.h> //assert

#endif //DEBUG_H