STDRZR is a collection of C++ classes used in many of my projects.
It is now freely available under the included license.

This collection consists of:

- TextToken a key-value parser for .acf files used by Valve
- Extended std basic_bytes and streams in Bytes.h
- Debug.h for finding memory leaks in windows applications (not in namespace stdrzr)